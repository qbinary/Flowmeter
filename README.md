# Flowmeter for ESP32

## Overview
This ESP32 firmware interfaces with a 7-segment display and a flow sensor to accurately measure, display, and transmit water flow data. Utilizing true parallel computing capabilities of the ESP32's dual cores, one thread is dedicated to reading sensor values for precise measurements, while the other manages display updates and MQTT communications. This design enhances data accuracy and real-time responsiveness, making the firmware ideal for smart home integration.

## Features
- **Real-Time Water Flow Measurement**: Captures and displays the volume of water passing through the sensor.
- **MQTT Integration**: Transmits live flow speed data and cumulative water usage to an MQTT broker, enabling remote monitoring and integration with home automation systems.
- **Energy-Efficient Design**: Employs efficient parallel computing strategies to optimize performance and accuracy.
- **Customizable Idle Time Posting**: Automatically posts total water consumption to MQTT after a predefined period of inactivity, defaulting to 10 seconds.
- **Display Management**: Uses a 7-segment display to provide immediate visual feedback on water consumption and flow rate.

## Hardware Requirements
- ESP32 microcontroller
- 7-segment display (TM1637)
- Flow sensor
- Basic wiring components (resistors, breadboard, etc.)

## Software Dependencies
- Arduino IDE for ESP32 development
- TM1637Display library for managing the 7-segment display
- PubSubClient library for MQTT communication
- WiFi library for ESP32

## Setup Instructions
1. **Configure WiFi and MQTT**: Update `secrets.h` with your WiFi SSID, password, and MQTT broker details.
2. **Hardware Setup**: Connect the flow sensor and 7-segment display to the ESP32 according to the pin configurations specified in the code.
3. **Firmware Deployment**: Compile and upload the firmware using the Arduino IDE.
4. **MQTT Subscription**: Subscribe to the defined MQTT topics to receive live updates on flow speed and water consumption.

### MQTT Topics
- `flowmeter/speed`: Publishes live flow speed.
- `flowmeter/lastAmount`: Reports cumulative water consumption after the idle period.

### Customization
- Modify capture intervals, display refresh rates, and MQTT topics as needed to fit your specific application requirements.

### Calibration of the Flow Sensor via Serial Monitor

To achieve accurate measurements with the ESP32 firmware, calibrating the flow sensor is crucial. Calibration involves determining the `rotations per liter` (RPL) value, which most times varies depending on flow speed. This section guides you through using the firmware's debug outputs for calibration.

##### Preliminary Steps

1. **Setup**: Ensure your ESP32 setup is connected to a computer with the Arduino IDE Serial Monitor open.
2. **Start Serial Monitor**: Set the baud rate to 115200 to match the firmware's configuration (`Serial.begin(115200);`).

##### Calibration Process

1. **Begin Measurement**: With the Serial Monitor open, let water flow through the sensor at a steady rate. The firmware will output various debug information, including counts and time intervals.
2. **Record Data**: Conduct tests at different flow rates, letting a known volume of water (e.g., 1 liter) pass through the sensor each time. For each test, note the count values (`count`) and the total milliliters calculated (`totalMl`) from the Serial Monitor.
3. **Calculate RPL**: For each flow rate, calculate the RPL value using the formula:
   \[ \text{RPL} = \frac{\text{Count}}{\text{Volume in Liters}} \]
   Ensure to perform this calculation for different flow speeds to map speed to RPL values accurately.

##### Adjusting Calibration Data

The firmware uses a compensation curve defined by datapoints in `DATAPOINTS_REVERSED` array, which maps speed to RPL. Based on your calibration results:

1. **Update Data Points**: Adjust the `DATAPOINTS_REVERSED` array with your calculated RPL values corresponding to specific flow speeds. These values are critical for the firmware to accurately convert raw sensor counts into liters per minute.
2. **Recompile and Upload**: After adjusting the calibration data, recompile and upload the firmware to the ESP32.

##### Important Notes

- **Consistency**: Ensure consistent flow rates during calibration to get accurate RPL values.
- **Recalibration**: If you change the capture rate (`CAPTURE_FREQUENCY_HZ`) or any other parameter affecting measurement timing, recalibrate the sensor as these changes can impact the flow speed readings.
- **Debugging Output**: The Serial Monitor outputs include `totalCount`, `dataCount`, and calculated milliliters (`totalMl`). Use these for fine-tuning your calibration.

Using the Serial Monitor's debug outputs allows for precise calibration of the flow sensor, ensuring accurate water usage measurements. Adjust your `DATAPOINTS_REVERSED` array based on calibration data to enhance measurement accuracy in your smart home water monitoring system.

#### Calibration for Boundary Speed Values

For the linear interpolation to function correctly across the entire range of flow speeds, it's important to set specific `rotations per liter` (RPL) values for the boundary speeds of 0 and 100 in the `DATAPOINTS_REVERSED` array. This ensures the interpolation curve extends smoothly into these end ranges.

##### Setting Boundary Values

- **Speed 0**: The RPL value for a speed of 0 should match the RPL of its immediate successor in the `DATAPOINTS_REVERSED` array. This creates a flat, horizontal extension of the interpolation curve at the start, ensuring accurate low-speed measurements.
  
- **Speed 100**: Conversely, the RPL value for a speed of 100 should match the RPL of its immediate predecessor. This adjustment ensures the curve extends linearly beyond the highest measured speed, maintaining accuracy at higher flow rates.

##### Practical Example

Given the calibration data in `DATAPOINTS_REVERSED` as:

```cpp
const unsigned int DATAPOINTS_REVERSED[DATAPOINT_COUNT][2] = {{0, 468}, {5, 468}, {11, 456}, {100, 456}};
```

- The speed of `0` has an RPL of `468`, mirroring its next speed (`5`) to ensure a consistent measurement starting point.
- The speed of `100` has an RPL of `456`, identical to its previous speed (`11`), to linearly extend the curve for any flow rate exceeding the highest calibrated speed.

##### Adjusting Your Calibration

1. **Initial Calibration**: Follow the calibration steps to determine RPL values for various speeds.
2. **Boundary Adjustment**: Ensure the lowest (0) and highest (100) speeds in your `DATAPOINTS_REVERSED` array are set as described, to maintain a consistent interpolation curve.
3. **Recompilation and Testing**: After adjusting these boundary values, recompile and upload the firmware. Conduct tests to verify that the flow measurements are accurate across the entire speed range.

# Help
If you have any questions or need help, you can reach me via **public@qbin.dev**  
Have a nice day!