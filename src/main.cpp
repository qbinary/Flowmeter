#include <Arduino.h>
#include <TM1637Display.h>
#include <WiFi.h>
#include <PubSubClient.h>
#include "secrets.h"

// Helper
const int SECOND_MS = 1000;
const uint64_t SECOND_MICROS = 1000000;

//// Config values
// Capturing
const uint64_t CAPTURE_INTERVAL_TIME_MICROS = 250000; // The time of one capturing session
const uint64_t CAPTURE_FREQUENCY_HZ = 500;            // The frequency used to read the sensor values in one interval
const int DISPLAY_UPDATE_FREQ_HZ = 10;
// const int INPUT_PIN = D0;           // Flow sensor input pin
const int INPUT_PIN = 17;           // Flow sensor input pin
const long DISCARD_TIME_MS = 10000; // Time in ms until meassured value will be resetted

// Connectivity
const String CLIENT_ID = "flowmeter" + String(ESP.getEfuseMac()).substring(0,4);
const String WIFI_SSID = SEC_WIFI_SSID;
const String WIFI_PASSWORD = SEC_WIFI_PASSWORD;

const String BROKER_IP = SEC_BROKER_IP;
const uint16_t BROKER_PORT = SEC_BROKER_PORT;

const String BASE_TOPIC = CLIENT_ID + "/";
const String SPEED_TOPIC = BASE_TOPIC + "speed";
const String LAST_AMOUNT_TOPIC = BASE_TOPIC + "lastAmount";

// Compensation data
// PERFECT 11:421
// PERFECT 5:433
const unsigned int DATAPOINT_COUNT = 4;

// [0] speed 
// [1] rotations per liter 
const unsigned int DATAPOINTS_REVERSED[DATAPOINT_COUNT][2] = {{0, 468}, {5, 468}, {11, 456}, {100, 456}};

//// Pre calculated values
const uint64_t INTERVAL_QUOTIENT_MICROS = SECOND_MICROS / CAPTURE_INTERVAL_TIME_MICROS;                    // Used to determine delay intervals by HZ for a specific time interval
const uint64_t CAPTURE_INTERVAL_COUNT = (SECOND_MICROS / CAPTURE_FREQUENCY_HZ) / INTERVAL_QUOTIENT_MICROS; // The amount of equaly long intervals for specified polling frequency
const uint64_t CAPTURE_INTERVAL_TIMEOUT = CAPTURE_INTERVAL_TIME_MICROS / CAPTURE_INTERVAL_COUNT;           // The timeout for each polling interval

//// Configuration
// 7 segment display
// const int DISP_CLK = D6;
// const int DISP_IO = D5;
const int DISP_CLK = 22;
const int DISP_IO = 21;
TM1637Display display(DISP_CLK, DISP_IO);

// MQTT
WiFiClient wifiClient;
PubSubClient mqtt(wifiClient);

//// Runtime variables
// Core
unsigned int totalMl = 0;            // Total millilitres counted; will be resetted after DISCARD_TIME_MS
unsigned long lastUpdate = millis(); // Last time a value was added to totalMl; used for the discard timer
unsigned long lastCount = 0;

// Benchmarking
unsigned int dataCount = 0;  // Count of capturing intervals since last discard; used for benchmarking, debugging and datapoint configuration
unsigned int totalCount = 0; // Total captured count since last discard; used for benchmarking, debugging and datapoint configuration

// Thread control
bool publisherLocked = false;

bool ensureConnectivity()
{
  if (WiFi.status() != WL_CONNECTED)
  {
    Serial.println("Wifi not connected!");
    return false;
  }

  if (!mqtt.connected())
  {
    Serial.println("MQTT not connected!");
    mqtt.connect(CLIENT_ID.c_str());
    Serial.println(mqtt.state());
  }
  return mqtt.connected();
}

void displayAmount(int ml)
{
  int litres = round(ml / 1000);
  int leftoverMl = ml % 1000;
  if (ml == 0) {
    display.clear();
  }
  else if (ml < 10000)
  {
    display.showNumberDec(ml);
  }
  else
  {
    display.showNumberDecEx(litres, 0b01000000, false, 2, 0);
    int dispMl = leftoverMl / 10;
    display.showNumberDecEx(dispMl, 0b01000000, dispMl < 10, 2, 2);
  }
}

/**
 * Return whether the chip could reach the frequency
 */
unsigned int captureInterval(uint64_t intervalQuotientMicros, uint64_t intervalCount, uint64_t intervalDelay)
{
  // Capture
  int targetExceeded = 0;

  bool lastInput = digitalRead(INPUT_PIN);
  bool currentInput = lastInput;

  unsigned int count = 0;
  for (uint64_t currentInterval = 0; currentInterval < intervalCount; currentInterval++)
  {
    uint64_t startTime = micros();

    // Capturing
    currentInput = digitalRead(INPUT_PIN);
    if (currentInput && currentInput != lastInput)
    {
      count++;
    }
    lastInput = currentInput;

    // Wait
    uint64_t deltaTime = micros() - startTime;
    if (deltaTime > intervalDelay)
      targetExceeded++;
    else
      delayMicroseconds(intervalDelay - deltaTime);
  }

  // Error message
  if (targetExceeded > 0)
    Serial.println("Capturing interval exceeded");

  return count;
}

double linearInterpolation(double x1, double y1, double x2, double y2, double x)
{
  double m = (y2 - y1) / (x2 - x1);
  double b = y1;
  return m * (x - x1) + b;
}

unsigned int compensatedCountToMl(unsigned int count)
{
  // Quick return; if count is 0
  if (count == 0)
    return 0;

  // Rotations per litre
  double dcount = count;
  double rpl = -1;

  // Determine correct compensation curve section
  for (unsigned int i = 1; i < DATAPOINT_COUNT; i++)
  {
    unsigned int x1 = DATAPOINTS_REVERSED[i - 1][0];
    unsigned int y1 = DATAPOINTS_REVERSED[i - 1][1];

    unsigned int x2 = DATAPOINTS_REVERSED[i][0];
    unsigned int y2 = DATAPOINTS_REVERSED[i][1];

    // If count is in data range: interpolate and break
    if (count < x2)
    {
      rpl = linearInterpolation(x1, y1, x2, y2, dcount);
      break;
    }
  }

  // If no fitting rpl found -> use last
  if (rpl == -1)
    rpl = DATAPOINTS_REVERSED[DATAPOINT_COUNT - 1][1];

  // Calculate ml per rotation from rotations per litre
  double mlpr = 1000. / rpl;

  // Calculate actual ml amount
  unsigned int retVal = round(dcount * mlpr);

  return retVal;
}

TaskHandle_t publisherTask;
void publisherThread(void *params)
{
  Serial.print("Publisher thread running on core ");
  Serial.println(xPortGetCoreID());
  while (true)
  {
    // feedLoopWDT();
    if (!publisherLocked)
    {

      // Display
      displayAmount(totalMl);

      // Publish to mqtt
      if (ensureConnectivity())
      {
        if (lastCount != 0)
          mqtt.publish(SPEED_TOPIC.c_str(), String(lastCount).c_str(), true);
      }

      publisherLocked = true;
    }
    delay(SECOND_MS / DISPLAY_UPDATE_FREQ_HZ);
  }
}

void setup()
{
  // Open serial debugging connection
  Serial.begin(115200);
  Serial.println("serial begin");

  // Configure IO
  pinMode(INPUT_PIN, INPUT_PULLDOWN);
  display.setBrightness(1);

  // Configure connectivity
  WiFi.begin(WIFI_SSID.c_str(), WIFI_PASSWORD.c_str());
  mqtt.setServer(BROKER_IP.c_str(), BROKER_PORT);
  // mqtt.connect(String(ESP.getEfuseMac()).c_str());
  ensureConnectivity();

  xTaskCreatePinnedToCore(publisherThread, "publisher", 10000, NULL, tskIDLE_PRIORITY, &publisherTask, 0);
}

void loop()
{
  // Handle litre counting
  unsigned int count = captureInterval(INTERVAL_QUOTIENT_MICROS, CAPTURE_INTERVAL_COUNT, CAPTURE_INTERVAL_TIMEOUT);

  if (count != 0)
  {
    lastUpdate = millis();
    dataCount++;

    totalCount += count;
    totalMl += compensatedCountToMl(count);
  }
  lastCount = count;

  // Print litre conversion
  // Serial.printf("cnt: %d; Sigma-cnt:%d ml: %d\n", count, totalCount, totalMillis);

  if (millis() - lastUpdate > DISCARD_TIME_MS)
  {
    if (dataCount > 1)
    {
      double countAvg = totalCount / dataCount;
      Serial.printf("dataCount: %d; countAvg: %f; calculatedMillis: %d; totalCount: %d \n", dataCount, countAvg, totalMl, totalCount);
      if (ensureConnectivity())
      {
        mqtt.publish(LAST_AMOUNT_TOPIC.c_str(), String(totalMl).c_str());
      }
    }
    totalMl = 0;
    totalCount = 0;
    dataCount = 0;
  }

  // Unlock publisher
  publisherLocked = false;
}